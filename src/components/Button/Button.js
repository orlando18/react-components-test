import React from "react";
import styles from "./Button.module.css";

export const Button = ({ text }) => {
  return <div className={styles.button}>{text}</div>;
};
